﻿using System;
using UnityEngine;

public class GameEvent : MonoBehaviour
{
    public static GameEvent instance;

    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(gameObject);
    }

    public event Action Camera_BtnClick;
    public event Action Jump_BtnClick;
    public event Action WinUI;

    public void Camera_Btn()
    {
        Camera_BtnClick?.Invoke();
    }

    public void Jump_Btn()
    {
        Jump_BtnClick?.Invoke();
    }

    public void WinUI_Display()
    {
        WinUI?.Invoke();
    }
}
