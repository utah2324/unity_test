﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinUIDisplay : MonoBehaviour
{
    private void Start()
    {
        GameEvent.instance.WinUI += SetActive;
    }

    private void OnDestroy()
    {
        GameEvent.instance.WinUI -= SetActive;
    }

    private void SetActive()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        Time.timeScale = 0; // end gem
    }

}
